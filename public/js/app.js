'use strict'

//	Variables globales
var xmlHttp     = new XMLHttpRequest(), 
    container   = document.querySelector('.palette'), 
    arrColor    = ["#000000", "#FFFFFF", "#FF0000", "#00FF00", "#0000FF", 
                    "#FFFF00", "#00FFFF", "#FF00FF", "#C0C0C0", "#808080", 
                    "#800000", "#808000", "#008000", "#008000", "#008080", 
                    "#000080", "#556B2F", "#FF7416", "#7CFC00", "#7FFF00", 
                    "#B8860B", "#FFD700", "#DAA520", "#EEE8AA", "#BDB76B", 
                    "#F0E68C", "#DC143C", "#A52A2A", "#FF6347", "#CD5C5C", 
                    "#E9967A", "#FA8072", "#FF4500", "#FF8C00", "#FFA500", 
                    "#00FFFF", "#40E0D0", "#008080", "#008B8B", "#20B2AA", 
                    "#48D1CC", "#7FFFD4", "#B0E0E6", "#97CE68", "#EB9532", 
                    "#EE543A", "#D8335B", "#953163", "#5F9EA0", "#4682B4", 
                    "#6495ED", "#00BFFF", "#1E90FF", "#ADD8E6", "#191970", 
                    "#000080", "#4169E1", "#8A2BE2", "#6A5ACD", "#9370DB", 
                    "#9400D3", "#800080", "#D8BFD8", "#FF69B4", "#FF1493", 
                    "#FFE4C4", "#FFF8DC", "#FAFAD2", "#8B4513", "#D2691E", 
                    "#F4A460", "#CD853F", "#E6E6FA", "#FFFAFA", "#F0FFF0"
                    ]

function updateProgress (evt) {
    console.log('Progreso')
    console.log(evt)
}
function transferComplete (evt) {
    console.log('Load')
    console.log(evt)
}
function transferFailed (evt) {
    console.log('error')
    console.log(evt)
}
function transferCanceled (evt) {
    console.log('abort')
    console.log(evt)
}

xmlHttp.onreadystatechange = function () {
    if (xmlHttp.readyState == XMLHttpRequest.DONE) {
        console.log(xmlHttp.responseText)
    }
}

document.querySelector('#connect').addEventListener('click', function () {
    xmlHttp.open("GET", '/conectar');
    xmlHttp.send(null);
})

var fillColors = function () {
    var tmp = ''
    arrColor.some(function (element, index, arr) {
        tmp += '<div style="background:'+ element +'" id="'+ element +'"></div>'
    })
    container.innerHTML = tmp

    //  Luego de llenar los divs con los colores le agrego un evento a cada div
    var divs = document.querySelectorAll('.palette div')
    console.log(divs)
    var color, 
        i = 0 

    for (i; i < divs.length; i++) {
        divs[i].addEventListener('click', function (e) {
            color = this.id.split('#')[1]

            ajaxCalls(color)
        })
    }
    //divs.some(function (element, index, arr) {
    //    console.log(this)
        //.addEventListener('click', function (e) {console.log(this)})
    //})
}

document.addEventListener('DOMContentLoaded', function () {
    fillColors()
})

var ajaxCalls = function (color) {
    console.log(color)

    xmlHttp.open("GET", '/color/' + color, true);
    xmlHttp.send(null);
}

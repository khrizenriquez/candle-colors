'use strict'

const express 	= require('express'), 
	path 		= require('path'), 
	port 		= 3000

let pb, 
    Playbulb    = require('./playbulb'), 
    app 		= express(), 
    noble       = require('noble'), 
    defaultColor = [255, 255, 255]

//	Middleware para servir archivos estáticos
app.use('/static', express.static(path.join(__dirname, 'public')))
//app.use(express.static('public'))

function customHeaders (req, res, next) {
  	// Switch off the default 'X-Powered-By: Express' header
  	app.disable( 'x-powered-by' )

  	res.setHeader( 'X-Powered-By', 'Chris - Candle v0.0.1' )

  	next()
}

//	Función para cambiar los colores hexadecimales a rgb
var hexToRgbA = function (hex) {
    let c
    if(/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
        c = hex.substring(1).split('')
        if (c.length == 3) {
            c= [c[0], c[0], c[1], c[1], c[2], c[2]]
        }
        c = '0x' + c.join('')

        return [(c>>16)&255, (c>>8)&255, c&255].join('-')
    }
    throw new Error('Bad hex')
}

app.use(customHeaders);

//	Middleware para servir las vistas

//	Peticiones rest
app.get('/', (req, res) => {
	res.sendFile(path.join(`${__dirname}/views/index.html`))
})
app.get('/conectar', (req, res) => {
	pb = new Playbulb.PlaybulbCandle()

    pb.ready(function () {
    	console.log('Conectado')

    	//	Color por defecto
    	pb.setColor(0, defaultColor[0], defaultColor[1], defaultColor[2])
        res.sendStatus(200)
    })
})

app.get('/color/:color', (req, res) => {
	let color = req.params.color, 
		splitColor

	if (color === undefined) {
		res.sendStatus(404)

		return false
	}

	let rgbColor = hexToRgbA('#' + color)
	splitColor = rgbColor.split('-')
	console.log(pb)
	console.log(splitColor)

	if (pb === undefined) {
		res.sendStatus(404)
	
		return false
	}

	pb.setColor(0, parseInt(splitColor[0], 10), 
					parseInt(splitColor[1], 10), 
					parseInt(splitColor[2], 10))

	res.sendStatus(200)

	return false
})


//	
app.listen(port, () => {
	console.log(`Listening on port ${port}`)
})
